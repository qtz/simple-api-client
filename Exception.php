<?php
/**
 * Exception.php
 * @author Revin Roman http://phptime.ru
 */

namespace simple\api;

/**
 * Class Exception
 * @package simple\api
 */
class Exception extends \Exception
{
}