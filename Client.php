<?php
/**
 * Client.php
 * @author Revin Roman http://phptime.ru
 */

namespace simple\api;

/**
 * Class Client
 * @package simple\api
 */
class Client
{

    /** @var string урл используемого api */
    public $api_url = 'http://myproject.lc';

    /** @var string ID приложения */
    public $client_id = null;

    /** @var string секретный ключ приложения */
    public $client_secret = null;

    /** @var array список дополнительных заголовков */
    public $headers = [];

    /** @var string юзерагент запросов клиента */
    public $user_agent = 'Simple API PHPClient v1.2.2';

    /** @var array токен доступа */
    private $token = null;

    /**
     * @param string $api_url урл используемого хаба
     * @param string $client_id ID приложения
     * @param string $client_secret секретный ключ приложения
     */
    public function __construct($api_url = null, $client_id = null, $client_secret = null)
    {
        if (null !== $api_url) {
            $this->api_url = $api_url;
        }

        if (null !== $client_id) {
            $this->client_id = $client_id;
        }

        if (null !== $client_secret) {
            $this->client_secret = $client_secret;
        }
    }

    /**
     * Метод проверяет, является ли ответ от сервера ответом об ошибке
     * @param string $response
     * @return bool
     */
    public static function isErrorResponse($response)
    {
        return isset($response['type']) && isset($response['message']);
    }

    /**
     * Добавить ко всем запросам из клиента дополнительный заголовок
     * @param string $name название заголовка
     * @param string $value значение заголовка
     * @return self
     */
    public function addHeader($name, $value)
    {
        $this->headers[$name] = $value;

        return $this;
    }

    /**
     * Геттер параметра headers
     * @return array
     */
    public function getHeaders()
    {
        return $this->headers;
    }

    /**
     * Метод возвращает массив заголовков в формате curl
     * @return array
     */
    public function getCurlHeaders()
    {
        $result = [];
        $headers = $this->headers;
        foreach ($headers as $name => $value) {
            $result[] = sprintf('%s: %s', $name, $value);
        }
        return $result;
    }

    /**
     * Метод отправляет GET запрос к api хаба
     * @param string $api_method название метода api (system, user, order/element, client/staff)
     * @param array $params массив параметров запроса
     * @return string
     * @throws Exception
     */
    public function GET($api_method, $params = [])
    {
        return $this->send('GET', $api_method, $params);
    }

    /**
     * Метод отправляет POST запрос к api хаба
     * @param string $api_method название метода api (system, user, order/element, client/staff)
     * @param array $params массив параметров запроса
     * @return string
     * @throws Exception
     */
    public function POST($api_method, $params = [])
    {
        return $this->send('POST', $api_method, $params);
    }

    /**
     * Метод отправляет PUT запрос к api хаба
     * @param string $api_method название метода api (system, user, order/element, client/staff)
     * @param array $params массив параметров запроса
     * @return string
     * @throws Exception
     */
    public function PUT($api_method, $params = [])
    {
        return $this->send('PUT', $api_method, $params);
    }

    /**
     * Метод отправляет DELETE запрос к api хаба
     * @param string $api_method название метода api (system, user, order/element, client/staff)
     * @param array $params массив параметров запроса
     * @return string
     * @throws Exception
     */
    public function DELETE($api_method, $params = [])
    {
        return $this->send('DELETE', $api_method, $params);
    }

    /**
     * Метод отправляет запрос к api хаба
     * @param string $http_method HTTP метод запрос (GET, POST, PUT, DELETE)
     * @param string $api_method название метода api (system, user, order/element, client/staff)
     * @param array $params массив параметров запроса
     * @return string
     * @throws Exception
     */
    public function send($http_method, $api_method, $params = [])
    {
        $url = $this->api_url . '/api/' . $api_method;

        $this->addHeader('Authorization', 'Bearer ' . $this->token);

        $curl_options = [
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_USERAGENT => $this->user_agent,
            CURLOPT_HTTPHEADER => $this->getCurlHeaders(),
        ];

        $http_method = mb_strtoupper($http_method);

        switch ($http_method) {
            default:
            case 'GET':
            case 'DELETE':
                $curl_options[CURLOPT_CUSTOMREQUEST] = $http_method;
                $url .= '?' . http_build_query($params);
                break;
            case 'POST':
                $curl_options[CURLOPT_POST] = true;
                $curl_options[CURLOPT_POSTFIELDS] = http_build_query($params, '', '&', PHP_QUERY_RFC3986);
                $curl_options[CURLOPT_HTTPHEADER][] = 'Content-type: application/x-www-form-urlencoded';
                break;
            case 'PUT':
                $body = json_encode($params);

                $fp = fopen('php://temp/maxmemory:256000', 'w');
                if (!$fp) {
                    throw new Exception('Не удалось создать временный файл в памяти');
                }
                fwrite($fp, $body);
                fseek($fp, 0);

                $curl_options[CURLOPT_PUT] = true;
                $curl_options[CURLOPT_BINARYTRANSFER] = true;
                $curl_options[CURLOPT_INFILE] = $fp;
                $curl_options[CURLOPT_INFILESIZE] = strlen($body);
                $curl_options[CURLOPT_HTTPHEADER][] = 'Content-type: application/json';
                break;
        }

        $ch = curl_init($url);
        if (false === $ch) {
            throw new Exception(sprintf(
                'Ошибка создания curl объекта - #%d %s',
                curl_errno($ch),
                curl_error($ch)
            ));
        }

        curl_setopt_array($ch, $curl_options);

        $response = json_decode(curl_exec($ch), true);

        curl_close($ch);

        return $response;
    }
}