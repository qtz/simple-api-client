# Клиент для API

В клиенте всего два метода:
* static::isErrorResponse() - Метод проверяет, является ли ответ от сервера ответом об ошибке
* send() - Метод отправляет запрос к апи хаба

Параметры методов прокоментированы в коде

## Примеры использования
```php
$Client = new \simple\api\Client();

$response = $Client->send('GET', 'system');
$response = $Client->send('POST', 'client', [
    'name' => 'ООО "Рога и Копыта"',
    'discount' => true,
]);
$response = $Client->send('DELETE', 'order/element', ['id' => 12]);

if(\simple\api\Client::isErrorResponse($response)){
    echo sprintf('Произошла ошибка - %s', $response['message']);
}
```